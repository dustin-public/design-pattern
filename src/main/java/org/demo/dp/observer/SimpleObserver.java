package org.demo.dp.observer;

import java.lang.invoke.MethodHandles;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SimpleObserver implements Observer {
    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
    
    private int value;
    private String name;
    
    public SimpleObserver(String n) {
	this.name = n;
    }

    public void update(int value) {
	this.value = value;
	display();
    }

    public void display() {
	log.info("name={}, value={}", name, value);
    }
}
