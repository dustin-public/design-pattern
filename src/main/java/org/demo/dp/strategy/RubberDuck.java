package org.demo.dp.strategy;

import org.demo.dp.strategy.behavior.FlyBehavior;
import org.demo.dp.strategy.behavior.QuackBehavior;
import org.demo.dp.strategy.behavior.impl.FlyNoWay;
import org.demo.dp.strategy.behavior.impl.Squeak;

public class RubberDuck extends Duck {

    public RubberDuck() {
	flyBehavior = new FlyNoWay();
	quackBehavior = new Squeak();
	// quackBehavior = () -> System.out.println("Squeak");
    }

    public RubberDuck(FlyBehavior flyBehavior, QuackBehavior quackBehavior) {
	this.flyBehavior = flyBehavior;
	this.quackBehavior = quackBehavior;
    }

    public void display() {
	System.out.println("I'm a rubber duckie");
    }
}
