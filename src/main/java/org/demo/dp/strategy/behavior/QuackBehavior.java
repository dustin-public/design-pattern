package org.demo.dp.strategy.behavior;

public interface QuackBehavior {
	public void quack();
}
