package org.demo.dp.strategy.behavior.impl;

import org.demo.dp.strategy.behavior.QuackBehavior;

public class Quack implements QuackBehavior {
    public void quack() {
	System.out.println("Quack");
    }
}
