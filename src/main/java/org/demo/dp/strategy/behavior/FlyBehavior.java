package org.demo.dp.strategy.behavior;

public interface FlyBehavior {
	public void fly();
}
