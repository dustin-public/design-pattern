package org.demo.dp.strategy.behavior.impl;

import org.demo.dp.strategy.behavior.FlyBehavior;

public class FlyNoWay implements FlyBehavior {
    public void fly() {
	System.out.println("I can't fly");
    }
}
