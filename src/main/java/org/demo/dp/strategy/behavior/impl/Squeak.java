package org.demo.dp.strategy.behavior.impl;

import org.demo.dp.strategy.behavior.QuackBehavior;

public class Squeak implements QuackBehavior {
    public void quack() {
	System.out.println("Squeak");
    }
}
