package org.demo.dp.strategy.behavior.impl;

import org.demo.dp.strategy.behavior.FlyBehavior;

public class FlyWithWings implements FlyBehavior {
    public void fly() {
	System.out.println("I'm flying!!");
    }
}
