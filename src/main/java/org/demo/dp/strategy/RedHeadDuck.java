package org.demo.dp.strategy;

import org.demo.dp.strategy.behavior.impl.FlyWithWings;
import org.demo.dp.strategy.behavior.impl.Quack;

public class RedHeadDuck extends Duck {

    public RedHeadDuck() {
	flyBehavior = new FlyWithWings();
	quackBehavior = new Quack();
    }

    public void display() {
	System.out.println("I'm a real Red Headed duck");
    }
}
