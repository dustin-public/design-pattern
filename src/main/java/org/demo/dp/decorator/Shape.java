package org.demo.dp.decorator;

public interface Shape {
    void draw();
}
