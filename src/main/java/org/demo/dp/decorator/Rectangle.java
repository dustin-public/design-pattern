package org.demo.dp.decorator;

public class Rectangle implements Shape {

    @Override
    public void draw() {
	System.out.println("Rectangle:draw()");
    }
}