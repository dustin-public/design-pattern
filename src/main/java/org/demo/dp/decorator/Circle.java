package org.demo.dp.decorator;

public class Circle implements Shape {

    @Override
    public void draw() {
	System.out.println("Circle:draw()");
    }
}