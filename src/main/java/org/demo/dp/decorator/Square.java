package org.demo.dp.decorator;

public class Square implements Shape {

    @Override
    public void draw() {
	System.out.println("Square::draw()");
    }
}
