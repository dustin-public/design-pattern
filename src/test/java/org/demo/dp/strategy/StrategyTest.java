package org.demo.dp.strategy;

import org.junit.Test;

public class StrategyTest {

    @Test
    public void mallard() {
	Duck mallard = new MallardDuck();
	mallard.display();
	mallard.performQuack();
	mallard.performFly();
    }
    
    @Test
    public void rubber() {
	Duck rubber = new RubberDuck();
	rubber.display();
	rubber.performQuack();
	rubber.performFly();
    }
}
