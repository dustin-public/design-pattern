package org.demo.dp.observer;

import org.junit.Test;


public class ObserverTest {

    @Test
    public void observer() {
	SimpleSubject simpleSubject = new SimpleSubject();
	
	SimpleObserver o1 = new SimpleObserver("o1");
	SimpleObserver o2 = new SimpleObserver("o2");

	simpleSubject.registerObserver(o1);
	simpleSubject.registerObserver(o2);
	simpleSubject.setValue(80);
	
	simpleSubject.notifyObservers();
    }
}
