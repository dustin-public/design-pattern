package org.demo.dp.decorator.coffebiz;

import org.demo.dp.decorator.coffeebiz.Beverage;
import org.demo.dp.decorator.coffeebiz.DarkRoast;
import org.demo.dp.decorator.coffeebiz.Espresso;
import org.demo.dp.decorator.coffeebiz.HouseBlend;
import org.demo.dp.decorator.coffeebiz.Mocha;
import org.demo.dp.decorator.coffeebiz.Soy;
import org.demo.dp.decorator.coffeebiz.Whip;
import org.junit.Test;

public class CoffeeBizTest {

    @Test
    public void coffeeBiz() {
	Beverage beverage = new Espresso();
	System.out.println(beverage.getDescription() 
			+ " $" + beverage.cost());

	Beverage beverage2 = new DarkRoast();
	beverage2 = new Mocha(beverage2);
	beverage2 = new Mocha(beverage2);
	beverage2 = new Whip(beverage2);
	System.out.println(beverage2.getDescription() 
			+ " $" + beverage2.cost());

	Beverage beverage3 = new HouseBlend();
	beverage3 = new Soy(beverage3);
	beverage3 = new Mocha(beverage3);
	beverage3 = new Whip(beverage3);
	System.out.println(beverage3.getDescription() 
			+ " $" + beverage3.cost());
    }
}
