package org.demo.dp.decorator;

import org.junit.Test;

public class DecoratorTest {

    @Test
    public void decorator() {
	Shape circle = new Circle();

	Shape redCircle = new RedShapeDecorator(new Circle());

	Shape redRectangle = new RedShapeDecorator(new Rectangle());
	
	Shape redNested = new RedShapeDecorator(redCircle);
	//System.out.println("Circle with normal border");
	circle.draw();

	//System.out.println("\nCircle of red border");
	redCircle.draw();

	//System.out.println("\nRectangle of red border");
	redRectangle.draw();
	redNested.draw();
    }
}
